<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <?php wp_head();?>
</head>

<body>
  <div class="blog-masthead">
    <div class="container">
      <div class="row">
        <div class='col-sm-6 col-12 text-center-sm'>
          <a target="_blank" href="<?php echo get_option('landing'); ?>">
            <img id="logo" src="<?php echo get_template_directory_uri(); ?>/content/images/white-logo.png">
          </a>
        </div>
        <div class="col-md-5 col-sm-6 col-12 text-right text-center-sm">
          <a class="social_network_logo" title="Twitter" href="<?php echo get_option('twitter'); ?>" target="_blank">
            <i class="fab fa-twitter"></i>
          </a>
          <a class="social_network_logo" title="Facebook" href="<?php echo get_option('facebook'); ?>" target="_blank">
            <i class="fab fa-facebook-f"></i>
          </a>
          <a class="social_network_logo" title="Linkedin" href="<?php echo get_option('linkedin'); ?>" target="_blank">
            <i class="fab fa-linkedin"></i>
          </a>
          </div>
      </div>
    </div>
  </div>
