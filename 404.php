<?php get_header();?>
<div class="container text-center error-page">
    <h1 class="">Page non trouvée</h1>
    <p>Vous avez tout cassé...</p>
    <img id="broken-logo" src="<?php echo get_template_directory_uri(); ?>/content/images/broken-logo.png">
    <a class="btn btn-outline-primary" href="<?php echo get_bloginfo('wpurl'); ?>">
        <i class="icon-arrow-left"></i> Retour
    </a>
</div>
<?php get_footer();?>