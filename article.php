<div class="card">
    <a href="<?php echo wp_get_shortlink() ?>">
        <img alt="" class="card-img-top" src="<?php getTheFirstImage($post) ?>">
    </a>
    <div class="card-body">
        <h3 class="card-title">
            <a href="<?php echo wp_get_shortlink() ?>">
                <?php the_title();?>
            </a>
        </h3>
        <h4 class="card-subtitle mb-2 text-muted">
            <?php echo apply_filters( 'the_date', get_the_date(), get_option( 'date_format' ), '', '' );?>
            par <?php the_author();?>
        </h4>
        <div class="card-text">
            <?php
            	the_excerpt();
			?>
        </div>
        <a class="btn btn-outline-primary" href="<?php echo wp_get_shortlink() ?>">
            <span class="fas fa-book-open"></span>Lire l'article
        </a>
    </div>
</div>