</div> <!-- /.container -->
	<footer class="blog-footer">
	    <div class="return-top">
	    	<a href="#" data-smooth-scroll=true>
	    		<span class="far fa-arrow-alt-circle-up"></span>Retour en haut de page
	    	</a>
	    </div>
	    <div class="row">
	        <ul id="footer-menu" class="list-inline">
	            <li><a href="<?php echo get_option('landing'); ?>/About"><span>A Propos</span></a></li>
	            <li><a href="<?php echo get_option('landing'); ?>/JoinUs"><span>Nous Rejoindre</span></a></li>
	            <li><a href="<?php echo get_option('landing'); ?>/Press"><span>Presse</span></a></li>
	            <li><a href="<?php echo get_option('landing'); ?>/Contact"><span>Contact</span></a></li>
	            <li><span>|</span></li>
	            <li><a href="https://connect.onecub.com" target="_blank"><span>Solution entreprises</span></a></li>
	            <li><a href="https://connect.onecub.com/Consulting" target="_blank"><span>Conseil</span></a></li>
	            <li><a href="https://www.seraphin.legal/seraphin-formation/rgpd/" target="_blank"><span>Formation</span></a></li>
	        </ul>
	    </div>
	    <div class="row">
	        <ul id="footer-share" class="list-inline">
	            <li><a title="Twitter" href="<?php echo get_option('twitter'); ?>" target="_blank"><i class="fab fa-twitter"></i></a></li>
	            <li><a title="Facebook" href="<?php echo get_option('facebook'); ?>" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
	            <li><a title="Linkedin" href="<?php echo get_option('linkedin'); ?>" target="_blank"><i class="fab fa-linkedin"></i></a></li>
	        </ul>
	    </div>
	    <div class="row">
		    <ul id="footer-copyright" class="list-inline text-muted text-center">
		        <li class="text-bold"><span> © 2019 Onecub</span></li>
		        <li><span>|</span></li>
		        <li><a href="<?php echo get_option('landing'); ?>/TermsAndConditions"><span>CGU</span></a></li>
		        <li><span>|</span></li>
		        <li><a href="<?php echo get_option('landing'); ?>/PrivacyPolicy"><span>Politique de confidentialité</span></a></li>
		    </ul>
		</div>
    </footer>
  <?php wp_footer(); ?>
  </body>
</html>