<?php

function update_theme() {
    exec("cd " . get_template_directory() . " && \"C:/Program Files/Git/cmd/git\" pull");
    status_header(200);
    die();
}

function get_articles() {
    global $post;

    $args = $_POST['params'];    
    $args['post_type'] = 'post';

    $return = [];

    $ajax_query = new WP_Query($args);

    if ( $ajax_query->have_posts() ) : while ( $ajax_query->have_posts() ) : $ajax_query->the_post();
        ob_start();
        include(locate_template('article.php'));
        array_push($return, ob_get_clean());
    endwhile;
    endif;

    header('Content-Type: application/json');
    echo json_encode($return);
    die();
}

add_action( 'wp_ajax_get_articles', 'get_articles' );
add_action( 'wp_ajax_nopriv_get_articles', 'get_articles' );
add_action( 'admin_post_update_theme', 'update_theme' );
