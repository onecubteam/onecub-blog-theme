<?php 

include("utils/improved_trim_excerpt.php");
include("utils/truncate.php");
include("admin-ajax.php");

function startwordpress_scripts() {
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/content/styles/bootstrap.min.css', array(), '3.3.6' );
	wp_enqueue_style( 'bricklayer', get_template_directory_uri() . '/content/styles/bricklayer.min.css', array(), '0.4.2');
	wp_enqueue_style( 'font-awesome', 'https://use.fontawesome.com/releases/v5.8.1/css/all.css');
	wp_enqueue_style( 'blog', get_template_directory_uri() . '/content/styles/blog.css', array('bootstrap', 'bricklayer') );
	wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Karla');
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/scripts/bootstrap.min.js', array( 'jquery' ), '3.3.6', true );
	wp_enqueue_script( 'bricklayer', get_template_directory_uri() . '/scripts/bricklayer.min.js', array( 'jquery' ), '0.4.2', true );
	wp_enqueue_script( 'bricklayer.jquery', get_template_directory_uri() . '/scripts/bricklayer.jquery.min.js', array( 'bricklayer', 'jquery' ), '0.4.2', true );
	wp_enqueue_script( 'index', get_template_directory_uri() . '/scripts/index.js', array('jquery', 'bricklayer','bricklayer.jquery'));
	wp_localize_script('index', 'ajaxurl', admin_url( 'admin-ajax.php' ) );
}

// Custom settings
function custom_settings_add_menu() {
	add_menu_page( 'Paramètres', 'Paramètres', 'manage_options', 'custom-settings', 'custom_settings_page', null, 99 );
}

function custom_settings_page_setup() {
	add_settings_section( 'section', 'Définition des URLs', null, 'theme-options' );
	add_settings_field( 'twitter', 'Twitter', 'setting_twitter', 'theme-options', 'section' );
	add_settings_field( 'facebook', 'Facebook', 'setting_facebook', 'theme-options', 'section' );
	add_settings_field( 'linkedin', 'LinkedIn', 'setting_linkedin', 'theme-options', 'section' );
	add_settings_field( 'landing', 'Onecub - Page d\'accueil', 'setting_landing', 'theme-options', 'section' );
	register_setting('section', 'twitter');
	register_setting('section', 'facebook');
	register_setting('section', 'linkedin');
	register_setting('section', 'landing');
}

function setting_twitter() { ?>	<input type="text" name="twitter" id="twitter" value="<?php echo get_option( 'twitter' ); ?>" /> <?php }
function setting_facebook() { ?> <input type="text" name="facebook" id="facebook" value="<?php echo get_option( 'facebook' ); ?>" /> <?php }
function setting_linkedin() { ?> <input type="text" name="linkedin" id="linkedin" value="<?php echo get_option( 'linkedin' ); ?>" /> <?php }
function setting_landing() { ?> <input type="text" name="landing" id="landing" value="<?php echo get_option( 'landing' ); ?>" /> <?php }

// Create Custom Global Settings
function custom_settings_page() {
	include("utils/custom_settings_page.php");
}

function admin_css() {
	$admin_handle = 'admin_css';
	$admin_stylesheet = get_template_directory_uri() . '/content/styles/admin.css';
	wp_enqueue_style( $admin_handle, $admin_stylesheet );
}

function theme_commits_late() {
    exec("cd " . get_template_directory() . " && \"C:/Program Files/Git/cmd/git\" remote update");
    exec("cd " . get_template_directory() . " && \"C:/Program Files/Git/cmd/git\" status -uno 2>&1", $output, $return_var);
    if(strpos($output[1], "is up to date") != false)
		return 0;
	else {
		if(preg_match('/by (\d+) commit/', $output[1], $matches)){
			return (int) $matches[1];
		} else {
			return -1;
		}
	}
    return $return_var;
}

function getTheFirstImage($post) {
	if (has_post_thumbnail($post))
		echo get_the_post_thumbnail_url($post);
	else {
		$doc = new DOMDocument();
	    libxml_use_internal_errors(true);
		$doc->loadHTML($post->post_content);
		$images = $doc->getElementsByTagName("img");
		if(sizeof($images) > 0)
			echo $images[0]->attributes["src"]->value;
	}
}

function isLivreBlanc($post = null){
	if($post == null) {
		return is_category() && get_query_var('category_name') == "livreblanc";
	} else {
		return get_the_category($post->post_id)->slug == "livreblanc";
	}
}

add_action('pre_get_posts','alter_query');
 
function alter_query($query) {
	//gets the global query var object
	global $wp_query;
	if(!$wp_query->is_admin && get_query_var('category_name') != "livreblanc") {
		$query->set('category__not_in' , get_category_by_slug('livreblanc')->term_id);
	}
}

add_action('admin_print_styles', 'admin_css', 11 );
add_action( 'wp_enqueue_scripts', 'startwordpress_scripts' );
add_action( 'admin_menu', 'custom_settings_add_menu' );
add_action( 'admin_init', 'custom_settings_page_setup' );

add_theme_support( 'title-tag' );
add_theme_support( 'post-thumbnails' );

remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'improved_trim_excerpt');
