<?php /* Get header from header.php (wordpress integrated function) */ ?>
<?php get_header();?>

<div class="container <?php if(isLivreBlanc()) echo "livreblanc";?>">

    <div class="blog-header <?php if (!is_home()) echo "header-return";?>">
        <?php if (! is_home()) {?>
            <a class="return-link" href="<?php echo get_bloginfo('wpurl'); ?>">
                <span class="far fa-arrow-alt-circle-left"></span>Retour au blog
            </a>
        <?php }?>
        <h1 class="blog-title">
            <?php if (isLivreBlanc()) { ?>
                <a href="#">Livre blanc</a>
            <?php } else { ?>
                <a href="<?php echo get_bloginfo('wpurl'); ?>">Blog, actualités</a>
            <?php } ?>
        </h1>
        <h2 class="blog-subtitle text-muted">
            <?php   if (is_tag()) { ?>
                        tag : <?php echo single_tag_title("", false);
                    } else if (is_date()) { ?>
                        période :<?php echo single_month_title(" ");
                    } else if (!isLivreBlanc()){ ?>
                        nos derniers articles
            <?php   } ?>
        </h2>
    </div>

    <div class="row">
    	<div class="col-sm-12 blog-main">
            <div class="blog-sidebar col-12 offset-0 col-sm-6 offset-sm-6 col-md-4 offset-md-8">
                <?php /* Get sidebar from sidebar.php (wordpress integrated function) */ ?>
                <?php get_sidebar();?>
            </div>
            <div class="bricklayer">
        <?php
        $count = $wp_query->post_count;
        if($count == 0) { 
            // no article : two empty cards to let the menu on the right
            echo "<div class='empty-card'><span></span></div><div class='empty-card'><span></span></div>";
        }
        else if ($count == 1){
            // 1 article : 1 empty card to let the menu on the right
            the_post();
            get_template_part('article', get_post_format());
            echo "<div class='empty-card'><span></span></div>";
        } else {
            // 2+ articles : display them one by one
            $current_pos = 0;
            while (have_posts() && $current_pos < 10) {
                $current_pos += 1 ;
                the_post();
                get_template_part('article', get_post_format());
            }
        }
        ?>
            </div>
    	</div>
    </div>

</div>

<script>
    <?php /* describe query here to get parameters for ajax requests */ ?>
    var base_query = <?php echo json_encode($wp_query->query_vars); ?>;
</script>

<?php /* Get footer from footer.php (wordpress integrated function) */ ?>
<?php get_footer();?>