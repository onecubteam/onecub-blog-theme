<?php if(!isLivreBlanc()){ ?>
	<div class="sidebar-module">
		<a id="livreblanc" class="btn btn-outline-primary" href="<?php echo get_category_link(get_category_by_slug("livreblanc")->term_id); ?>">
			<span class="i fas fa-book-open"></span>
			Livre blanc
		</a>
	</div>
	<?php if (get_the_tag_list()) {?>
		<div class="sidebar-module">
			<h4>Tags</h4>
			<?php 
				$tags = get_tags(array(
					'hide_empty' => false
				));
				usort($tags, function($tag1, $tag2) {
					return $tag1->count < $tag2->count;
				});
				$tags = array_slice($tags, 0, 8);
				echo '<ol class="list-unstyled">';
				foreach ($tags as $tag) {
					if($tag->count > 0){
						$tag_link = get_tag_link( $tag->term_id );
						echo '<li><a href="'. $tag_link . '">' . $tag->name . '</a></li>';
					}
				}
				echo '</ol>';
			?>
		</div>
	<?php }?>
	<div class="sidebar-module">
	  <h4>Archives</h4>
	  <ol class="list-unstyled">
	    <?php wp_get_archives('type=monthly&limit=5');?>
	  </ol>
	</div>
<?php } ?>