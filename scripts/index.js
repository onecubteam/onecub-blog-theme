// Initialize
$ = jQuery;
var current_page = 1;
var currently_loading_articles = false;

function loadArticles(){
	currently_loading_articles = true;
	var params = {
		paged : current_page + 1,
		posts_per_page : base_query.posts_per_page,
		year : base_query.year,
		monthnum: base_query.monthnum,
		tag : base_query.tag,
		cat : base_query.cat
	}
	$.post(
	    ajaxurl,
	    {
	        'action': 'get_articles',
	        'params': params
	    },	
	    function(response){
            response.forEach(function(e){
            	$(".bricklayer").appendElements(e);
            });
			current_page ++;
			currently_loading_articles = false;
        }
	);
}

$(function(){
	$(".bricklayer").bricklayer();
	$(window).scroll(function() {
	    if($(window).scrollTop() + $(window).height() > $(".return-top")[0].offsetTop) {
	    	if(!currently_loading_articles)
		        loadArticles();
	    }
	});
	$('a[data-smooth-scroll="true"][href^="#"]').click(function(){
		var the_id = $(this).attr("href");
		if (the_id === '#') {
			$('html, body').animate({
				scrollTop:$("body").offset().top
			}, 'slow');
			return false;
		}
		$('html, body').animate({
			scrollTop:$(the_id).offset().top
		}, 'slow');
		return false;
	});
});