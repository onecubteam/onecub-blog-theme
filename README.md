# Thème du blog Onecub
## Quickstart - Mise en place d'un nouveau wordpress
### Environnement
Le serveur est sous Apache 2.4, la BDD sous MySQL.
Pour faciliter le processus, nous avons utilisé [XAMPP](https://www.apachefriends.org/fr/download.html) sur notre serveur.

### Installation et configuration de Wordpress
- Télécharger [Wordpress](https://wordpress.org/download) (à ce jour version 5.2.1)
- Créer une base sur MySql qui sera destinée au Wordpress ainsi qu'un compte utilisateur dédié à Wordpress
- Lancer le serveur et configurer le Wordpress avec notamment les accès à la nouvelle base
- Dans le dossier `wp-content/themes`, supprimer les thèmes et cloner ce dépôt
- Se rendre à l'adresse du serveur suivie de `/wp-admin`, aller dans l'onglet Apparences > Thèmes et sélectionner le thème "Blog Onecub"
- Paramétrer ensuite le blog dans les onglets "Réglages" et "Paramètres" de cette interface d'admin

## Structure du thème
### Les vues
#### index.php
La page d'index est la première page qui sera affichée, donc à l'adresse [blog.onecub.com](http://blog.onecub.com/)
Elle appelle le header (header.php) au début, la sidebar (sidebar.php) au milieu du contenu et le footer (footer.php) à la fin.
Elle est également appelée pour afficher des pages plus particulières :
- une page de tag (exemple: [blog.onecub.com/tag/rgpd](http://blog.onecub.com/tag/rgpd/))
- la page du livre blanc : [blog.onecub.com/category/livreblanc](http://blog.onecub.com/category/livreblanc/)
Quelle que soit la manière dont est appelée index.php, cette page affiche un ensemble d'articles (en version courte) sous formes de "cartes". Chaque aperçu article est affiché selon le template article.php

#### header.php
Le header contient uniquement le logo Onecub ainsi que 3 liens vers les réseaux sociaux qui sont paramétrables depuis la page Paramètres dans l'interface d'admin

#### footer.php
Le footer contient des liens vers le site de Onecub et vers les réseaux sociaux (également paramétrable depuis l'admin)

#### sidebar.php
La sidebar contient :
- un lien vers le livre blanc
- une partie "Tags" affichant les liens vers les tags les plus utilisés
- une partie "Archives" affichant les liens vers les 5 derniers mois d'articles
La sidebar ne s'affiche pas sur la page du livre blanc.

#### article.php
C'est le template qui est utilisé pour afficher l'aperçu d'un article sous forme de "carte".
Grâce à la fonction `getTheFirstImage()`, soit l'image attachée à l'article soit la première image du contenu de l'article.
Ensuite cette image est affichée, puis le titre de l'article, la date, l'auteur et un court résumé.

#### single.php
La page single.php affiche un article (quel qu'il soit) en entier : titre, date, auteur, tags et contenu complet

#### 404.php
Page d'erreur 404

### Le style
#### style.css
Style non utilisé, sauf pour les informations liées au thème qui seront affichées dans l'admin pour le choix des thèmes

#### content/styles/blog.css
Style principal du blog

#### content/styles/admin.css
Style de l'interface d'admin

### Les fonctions
#### function.php
Script php qui regroupe l'ensemble des fonctions ajoutées aux fonctions de base de wordpress ainsi que le paramètrage de ce wordpress.
On y trouve:
- la gestion d'inclusion des styles CSS et scripts JS
- la gestion de la page "Paramètres" de l'interface d'administration
- des fonctions utiles dans les vues

#### admin-ajax.php
On y trouve les appels HTTP : 
- la mise à jour du thème (via un git pull de ce dépôt)
- l'obtention d'articles (pour le chargement dynamique de la page index.php)

#### improved_trim_excerpt.php
Ce script contient une fonction qui permet, dans un fichier HTML, de
- n'accepter que certains tags, et enlever les autres (cf `excerpt_allowed_tags`)
- transformer certains tags en d'autres (cf `excerpt_replacement_tags`)
Elle est utilisée pour le contenu des articles.

#### truncate.php
Ce script contient une fonction qui permet de raccourcir un texte en HTML sans couper de mots.
Elle est utilisée pour raccourcir le contenu des articles sur index.php.

### Les scripts
#### scripts/index.js
Ce script est essentiellement composé de 2 parties:
- l'initialisation du `bricklayer` qui permet d'avoir une gestion automatique et dynamique des colonnes de cartes sur index.php
- le chargement dynamique des cartes avec des appels AJAX