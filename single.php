<?php 
get_header(); // Affiche le header
the_post();	// Récupère les données de l'article
?>

<div class="container">
	<div class="blog-header header-return">
        <a class="return-link" href="<?php echo get_bloginfo('wpurl'); ?>">
            <span class="far fa-arrow-alt-circle-left"></span>Retour au blog
        </a>
	    <h1 class="blog-title"><?php the_title();?></h1>
        <p class="post-info text-center">
			Posté le <?php the_date();?> par <?php the_author();?>.
		</p>
		<p class="post-info text-center">
			<?php
				if (has_tag())
					foreach (get_the_tags() as $tag) {
    		?>
			<a href="<?php echo get_tag_link($tag->term_id) ?>">
				#<?php echo $tag->name; ?>
			</a>
			<span class='last-hidden'>, </span>
			<?php } ?>
		</p>
	</div>
	<div class="row">
		<div class="col-sm-12 blog-main blog-single">
			<?php the_content();?>
		</div>
	</div>
</div>
<?php get_footer();?>