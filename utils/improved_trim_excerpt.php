<?php

define("excerpt_allowed_tags", array('p', 'ol', 'ul', 'li', 'a', 'span', 'b', 'strong'));
define("excerpt_replacement_tags", array(
	'h2' => 'b',
	'h3' => 'b',
	'h4' => 'b',
	'h5' => 'b',
	'h6' => 'b'
));
define("excerpt_length", 350);

function replace_start_tags($string)
{
	return preg_replace_callback('/\<('.join('|', array_keys(excerpt_replacement_tags)).')/',
		function($matches) {
			$key = $matches[1];
			return array_key_exists($key, excerpt_replacement_tags) 
				? '<'.excerpt_replacement_tags[$key]
				: ''
				;
		}
		, $string);
}

function replace_end_tags($string)
{
	return preg_replace_callback('/\<\/('.join('|', array_keys(excerpt_replacement_tags)).')/',
		function($matches) {
			$key = $matches[1];
			return array_key_exists($key, excerpt_replacement_tags) 
				? '</'.excerpt_replacement_tags[$key]
				: ''
				;
		}
		, $string);
}

function improved_trim_excerpt( $text = '' ) {
	$raw_excerpt = $text;
	if ( '' == $text ) {
		$text = get_the_content( '' );
	}
	$text = strip_shortcodes( $text );
	$text = excerpt_remove_blocks( $text );

	/** This filter is documented in wp-includes/post-template.php */
	$text = apply_filters( 'the_content', $text );
	$text = preg_replace('@<script[^>]*?>.*?</script>@si', '', $text);
	$text = replace_start_tags($text);
	$text = replace_end_tags($text);
	$text = strip_tags($text, '<'.join('>,<', excerpt_allowed_tags).'>');
	
	$excerpt_more = apply_filters( 'excerpt_more', ' ' . '[&hellip;]' );

	/** approximative number of characters, without truncating mid-words */
	$text = truncate( $text, excerpt_length, array(
		'exact'=>false, 
		'html'=>'true', 
		'ending'=>'<span> [...]</span>'
	));
	return apply_filters( 'improved_trim_excerpt', $text, $raw_excerpt );
}