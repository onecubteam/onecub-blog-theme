<div class="wrap custom_settings_page">
	<h1>Paramètres</h1>
	<form method="post" action="options.php">
			<?php
			  	settings_fields( 'section' );
				do_settings_sections( 'theme-options' );
			  	submit_button();
			?>
	</form>
	<h2>Thème Onecub - Blog</h2>
	<?php if(!theme_commits_late()){ ?>
		<p>Le thème est à jour.</p>
	<?php } else { ?>
		<p>Une nouvelle version est disponible... <?php echo theme_commits_late(); ?> restant(s)</p>
		<form id="update_theme" method="post" action="admin-post.php">
			<input type="hidden" name="action" value="update_theme">
			<input type="submit" class="button" value="Mettre à jour">
		</form>
	<?php } ?>
</div>
<script>
	$ = jQuery;
	$("#update_theme").submit(function(e){
		e.preventDefault();
	    var form = $(this);
	    var url = form.attr('action');
	    $.ajax({
	           type: "POST",
	           url: url,
	           data: form.serialize(),
	           success: function(data) {
	               	window.location.reload();
	           }
        });
	});
</script>